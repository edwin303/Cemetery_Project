ESPAÑOL
--------------------------------------------------------------------------------------------------------------------------------
Este proyecto se trata de una aplicación de escritorio, orientada hacia un cementerio que registra y lleva el control de cuantos fallecidos hay, los familiares de esos fallecidos, el lote que ocupa y la fecha de registro. También se pueden hacer visualización de todos esos datos, así como búsquedas por diferentes filtros para llevar un mayor control sobre ellos. La aplicación hace uso de ficheros o archivos para guardar la información, el usuario es libre de elegir donde quiere que se guarde dicha información.
Esperamos que este proyecto te sea de utilidad.

ENGLISH
--------------------------------------------------------------------------------------------------------------------------------
This project is a desktop application, oriented towards a cemetery that registers and keeps track of how many deceased there are, the relatives of those deceased, the lot it occupies and the date of registration. You can also visualize all this data, as well as search through different filters to have greater control over them. The application makes use of files to save the information, the user is free to choose where he wants said information to be saved.
We hope this project is useful for you.
